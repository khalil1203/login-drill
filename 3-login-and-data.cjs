/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


const { promisify } = require('util');
const fs = require('fs');


const write = promisify(fs.writeFile);
const read = promisify(fs.readFile);
const unlink = promisify(fs.unlink);

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    const message = `${getCurrentTimestamp()} - User: ${user.name}, Activity: ${activity}\n`;
    return write('./ActivitiesLogging.log', message)
        .then(() => {
            console.log(`${activity} at ${getCurrentTimestamp()}`);
        });
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/








// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)




function answer1() {
    let file1 = "./file1.txt";
    let file2 = "./file2.txt";

    fs.writeFile(file1, "this is file1 data", (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log("file1 created successfully");
        }
    });
    fs.writeFile(file2, "this is file2 data", (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log("file2 created successfully");
        }
    });


    setTimeout(() => {
        fs.unlink(file1, (err) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log("file1 deleted successfully!!!");
            }
        });
        setTimeout(() => {
            fs.unlink(file2, (err) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log("file2 deleted successfully!!!!");
                }
            });
        }, 1000);
    }, 2000);

}

answer1();














// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 
// Using promise chaining



function answer2() {

    write("./lipsum.txt", 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.')
        .then(() => read("./lipsum.txt", 'utf8'))
        .then((data) => write("./lipsumCopy.txt", data))
        .then(() => unlink("./lipsum.txt"))
        .then(() => {
            console.log("Lipsum file created successfully!!!");
            console.log("Lipsum copy file created successfully!!!");
            console.log("Lipsum file deleted successfully!!!");


        })
        .catch((err) => {
            console.error(err);
        });

}


answer2();








// Q3.
// Use appropriate methods to 
// A. login with value 3 and call getData once login is successful
// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.

//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise.




function getCurrentTimestamp() {
    return new Date().toISOString();
}


function answer3() {


    login({ name: 'Khalil' }, 3)
        .then((user) => {
            logData(user, 'Login Success');
        })
        .catch((err) => {
            logData({ name: 'Unknown User' }, err.message)
        })
        .then(() => getData())
        .then((data) => logData({ name: 'Khalil' }, 'Playing football'))
        .catch(() => logData({ name: 'Khalil' }, 'Data is not obtained!!!'))
        .catch((err) => {
            console.log(err);
        });

}

answer3();